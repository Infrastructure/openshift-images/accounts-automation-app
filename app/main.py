#!/usr/bin/env python3

import os
import re
import requests
import uvicorn
import sentry_sdk

import app.helpers.gitlab.gitlab_helpers as glh
import app.helpers.ai.claude as claude
import app.helpers.glu.gnome_ldap_utils as glu

from fastapi import FastAPI, Header, HTTPException, Request
from sentry_sdk.integrations.fastapi import FastApiIntegration

sentry_sdk.init(
    dsn=os.getenv('SENTRY_DSN'),
    enable_tracing=True,
    traces_sample_rate=1.0,
    profiles_sample_rate=1.0,
    integrations=[
        FastApiIntegration(
            transaction_style="url",
        ),
    ],
    environment=os.getenv('ENVIRONMENT', 'development'),
)

app = FastAPI()

DOCS_TEXT = ('Please read documentation at '
             'https://handbook.gnome.org/infrastructure/developer-access.html '
             'and retry.')


@app.post("/webhooks/accounts")
async def webhook_accounts(request: Request, x_gitlab_token: str = Header(...)):
    with sentry_sdk.start_transaction(name="webhook_accounts"):
        if x_gitlab_token != os.getenv("GITLAB_WEBHOOK_TOKEN"):
            raise HTTPException(status_code=403)

        payload = await request.json()

        sentry_sdk.set_context("webhook", {
            "event_type": payload.get('event_type'),
            "project_id": payload.get('object_attributes', {}).get('project_id'),
            "issue_id": payload.get('object_attributes', {}).get('iid'),
        })

        if (
            payload['event_type'] != 'issue'
            or payload['object_attributes']['state'] == 'closed'
        ):
            raise HTTPException(status_code=400, detail="Event type not supported")

        gitlab_helpers = glh.GitlabHelpers(
            os.getenv('GITLAB_API_ENDPOINT'),
            os.getenv('GITLAB_TOKEN')
        )
        gitlab_priv_helpers = glh.GitlabHelpers(
            os.getenv('GITLAB_API_ENDPOINT'),
            os.getenv('GITLAB_ADMIN_TOKEN')
        )

        issue = gitlab_helpers.fetch_gitlab_issue(
            payload['object_attributes']['project_id'],
            payload['object_attributes']['iid']
        )

        if 'account request' in payload['object_attributes']['title'].lower():
            handle_account_request(
                issue, gitlab_helpers, gitlab_priv_helpers, payload
            )


@app.post("/webhooks/membership")
async def webhook_membership(request: Request, x_gitlab_token: str = Header(...)):
    with sentry_sdk.start_transaction(name="webhook_membership"):
        if x_gitlab_token != os.getenv("GITLAB_WEBHOOK_TOKEN"):
            raise HTTPException(status_code=403)

        payload = await request.json()

        sentry_sdk.set_context("webhook", {
            "event_type": payload.get('event_type'),
            "project_id": payload.get('object_attributes', {}).get('project_id'),
            "issue_id": payload.get('object_attributes', {}).get('iid'),
        })

        if (
            payload['event_type'] != 'issue' or
            payload['object_attributes']['state'] == 'closed'
        ):
            raise HTTPException(status_code=400, detail="Event type not supported")

        title = payload['object_attributes']['title'].lower()
        if any(x in title for x in ['renew', 'membership', 'application', 'emeritus']):
            handle_membership_request(payload)


def handle_account_request(issue, gitlab_helpers, gitlab_priv_helpers, payload):
    if 'Accounts' not in issue.labels:
        issue.labels.append('Accounts')
        issue.save()

    for note in gitlab_priv_helpers.list_confidential_notes(issue):
        if (
            all(x in note.body for x in ['action', 'git_access', 'module']) or
            'update_email' in note.body
        ):
            return

    desc = claude.generate_json(
        payload['object_attributes']['description'],
        issue
    )
    if not desc:
        return

    issue.notes.create({'body': str(desc), 'confidential': True})

    if desc['action'] == "git_access":
        handle_git_access(issue, gitlab_helpers, gitlab_priv_helpers, desc['module'])
    elif desc['action'] == 'update_email':
        handle_email_update(issue, gitlab_priv_helpers)


def handle_git_access(issue, gitlab_helpers, gitlab_priv_helpers, module):
    user = gitlab_priv_helpers.fetch_gitlab_user(
        issue.attributes['author']['id']
    )
    user_ldap = gitlab_priv_helpers.find_gitlab_ldap_identity(user.id)

    if user_ldap and 'has_ldap' not in issue.labels:
        issue.labels.append('has_ldap')
        issue.save()

    try:
        maint_url = ('https://maintainers-json-gitlab-cronjobs'
                     '.apps.openshift.gnome.org/')
        try:
            maint = requests.get(maint_url, timeout=5).json()[
                f"GNOME/{module}"
            ]['maintainers']
        except KeyError:
            maint = requests.get(maint_url, timeout=5).json()[
                f"GNOME/Incubator/{module}"
            ]['maintainers']

        text = (
            "The git_access action has been requested. "
            "Please vouch for this action to be performed by simply "
            "upvoting this issue with a thumbs up. \r\n\r\n"
            "CCed maintainers are: " +
            ', '.join(f"@{m['gitlab_username']}" for m in maint)
        )

        if not gitlab_helpers.gitlab_comment_already_submitted(issue, text):
            issue.notes.create({'body': text})

    except (KeyError, requests.exceptions.JSONDecodeError):
        text = (
            f"The module you specified does not exist or I could not "
            f"fetch maintainers list. {DOCS_TEXT}"
        )
        issue.notes.create({'body': text})


def handle_email_update(issue, gitlab_priv_helpers):
    user = gitlab_priv_helpers.fetch_gitlab_user(
        issue.attributes['author']['id']
    )
    user_ldap = gitlab_priv_helpers.find_gitlab_ldap_identity(user.id)

    if not user_ldap:
        text = (
            'There is no association between your GitLab account and a '
            f'GNOME Account.\r\n\r\nPlease login on GitLab using the '
            f'"GNOME Account" option and retry. {DOCS_TEXT}'
        )
        issue.notes.create({'body': text})
        return

    ldap_utils = setup_ldap_utils()
    gitlab_mail = get_gitlab_mail(user)

    if not gitlab_mail:
        return

    ldap_email = ldap_utils.get_attribute_from_person(
        'mail', gitlab_mail, 'uid'
    )

    if ldap_email:
        text = (
            f"Your email address is already in use by another user, "
            f"invalid request. {DOCS_TEXT}"
        )
        issue.notes.create({'body': text})
        return

    try:
        ldap_utils.replace_ldap_attr(user_ldap, 'mail', gitlab_mail)
    except glu.GnomeLdapUtilsException:
        text = ("There was an error updating your email address. "
                "We'll be in touch.")
        issue.notes.create({'body': text})
        return

    text = (
        f"Your secondary e-mail on GitLab ({gitlab_mail}) has been set "
        "to your GNOME Account e-mail successfully."
    )
    issue.notes.create({'body': text})

    issue.confidential = True
    issue.state_event = 'close'
    issue.save()


def setup_ldap_utils():
    return glu.GnomeLdapUtils(
        os.getenv('LDAP_GROUP_BASE'),
        os.getenv('LDAP_HOST'),
        os.getenv('LDAP_USER_BASE'),
        os.getenv('LDAP_USER'),
        os.getenv('LDAP_PASSWORD')
    )


def get_gitlab_mail(user):
    return next(
        (x.email for x in user.emails.list()
         if x.email.split('@')[1] != 'gnome.org'
         and x.email != user.attributes['email']),
        None
    )


def handle_membership_request(payload):
    gitlab_helpers = glh.GitlabHelpers(
        os.getenv('GITLAB_API_ENDPOINT'),
        os.getenv('GITLAB_TOKEN')
    )
    gitlab_priv_helpers = glh.GitlabHelpers(
        os.getenv('GITLAB_API_ENDPOINT'),
        os.getenv('GITLAB_ADMIN_TOKEN')
    )
    ldap_utils = setup_ldap_utils()

    issue = gitlab_helpers.fetch_gitlab_issue(
        payload['object_attributes']['project_id'],
        payload['object_attributes']['iid']
    )

    user = gitlab_priv_helpers.fetch_gitlab_user(
        issue.attributes['author']['id']
    )
    user_ldap = gitlab_priv_helpers.find_gitlab_ldap_identity(user.id)

    if user_ldap and 'has_ldap' not in issue.labels:
        issue.labels.append('has_ldap')
        issue.save()

    if 'Membership' not in issue.labels:
        issue.labels.append('Membership')
        issue.save()

        desc = payload['object_attributes']['description']
        matches = re.findall(r"@([a-zA-Z0-9._-]+(?:,|\s+|\)|$))", desc)

        if not matches:
            text = (
                'No existing GNOME Foundation members and/or GNOME '
                'maintainers/committers were quoted in issue description. '
                'Please quote (via @gitlab_username) at least one GNOME '
                'Foundation member and/or GNOME maintainer/committer to '
                'vouch for this request. Failure in doing so will result '
                'in this request to be denied.'
            )
            issue.notes.create({'body': text})
            return

        contacts = get_valid_contacts(
            matches, issue, gitlab_helpers, gitlab_priv_helpers, ldap_utils
        )

        if contacts:
            text = (
                f"{', '.join('@' + u for u in contacts)}, please vouch "
                f"for {payload['user']['name']} to be accepted as a "
                "GNOME Foundation Member."
            )
            if not gitlab_helpers.gitlab_comment_already_submitted(issue, text):
                issue.notes.create({'body': text})
        else:
            text = ('No quoted user(s) or quoted user(s) are not GNOME '
                    'Foundation Members or part of the GNOME GitLab group.')
            issue.notes.create({'body': text})


def get_valid_contacts(matches, issue, gitlab_helpers,
                       gitlab_priv_helpers, ldap_utils):
    contacts = []
    foundation = ldap_utils.get_group_from_ldap('foundation')

    for match in matches:
        username = match.strip(', \n . )')
        user = gitlab_helpers.fetch_gitlab_user_by_username(username)

        if not user or user.username == issue.author['username']:
            continue

        if (
            gitlab_helpers.user_in_gitlab_group(
                user, os.getenv('GNOME_PROJECT_ID')
            ) or gitlab_priv_helpers.find_gitlab_ldap_identity(user.id) in foundation
        ):
            contacts.append(user.username)

    return contacts


if __name__ == '__main__':
    uvicorn.run(app)
