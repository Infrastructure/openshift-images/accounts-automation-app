#!/usr/bin/python3

import ldap
import ldap.filter
import sys


class GnomeLdapUtils:
    def __init__(
        self,
        ldap_group_base,
        ldap_host,
        ldap_user_base,
        ldap_user,
        ldap_password,
        ldap_ca_path='/etc/ipa/ca.crt'
    ):
        self.ldap_group_base = ldap_group_base
        self.ldap_user_base = ldap_user_base
        self.ldap_user = ldap_user
        self.ldap_password = ldap_password
        self.ldap_host = ldap_host
        self.ldap_ca_path = ldap_ca_path

        try:
            ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, self.ldap_ca_path)
            self.conn = ldap.ldapobject.ReconnectLDAPObject(
                f"ldaps://{self.ldap_host}:636"
            )
            self.conn.simple_bind_s(self.ldap_user, self.ldap_password)
        except ldap.LDAPError as e:
            print(e, file=sys.stderr)
            sys.exit(1)

    def get_group_from_ldap(self, group):
        filter_str = ldap.filter.filter_format(
            '(&(objectClass=groupofnames)(cn=%s))', (group,)
        )
        results = self.conn.search_s(
            self.ldap_group_base, ldap.SCOPE_SUBTREE, filter_str, ('member',)
        )

        members = set()

        if len(results[0][1]) > 0:
            for _, attr in results:
                for userid in attr['member']:
                    splitentry = userid.decode('utf-8').split(',')
                    singleentry = splitentry[0]
                    splitteduid = singleentry.split('=')
                    uid = splitteduid[1]
                    members.add(uid)

        return members

    def get_attribute_from_person(self, filter_attr_key, filter_attr_value, attr):
        filter_str = ldap.filter.filter_format(
            '(&(objectClass=person)(%s=%s))',
            (filter_attr_key, filter_attr_value)
        )
        results = self.conn.search_s(
            self.ldap_user_base,
            ldap.SCOPE_SUBTREE,
            filter_str,
            (attr,)
        )

        if len(results) > 0:
            return results[0][0], results[0][1][attr][0].decode('UTF-8')
        return None

    def get_attributes_from_ldap(self, uid, attr, *attrs):
        results = []
        filter_str = ldap.filter.filter_format('(uid=%s)', (uid,))

        if len(attrs) > 0:
            attrs = list(attrs)
            attrs.insert(0, 'uid')
            attrs.insert(1, attr)
            _result = self.conn.search_s(
                self.ldap_user_base,
                ldap.SCOPE_SUBTREE,
                filter_str,
                tuple(attrs)
            )

            for arg in attrs:
                try:
                    results.append(_result[0][1][arg][0].decode('UTF-8'))
                except KeyError:
                    results.append(None)
        else:
            result = self.conn.search_s(
                self.ldap_user_base,
                ldap.SCOPE_SUBTREE,
                filter_str,
                ('uid', attr,)
            )

        if len(results) > 0:
            return results
        elif len(result) > 0:
            try:
                return result[0][1][attr][0].decode('UTF-8')
            except KeyError:
                return None
        return None

    def get_uids_from_group(self, group, excludes=None):
        if excludes is None:
            excludes = []
        people = self.get_group_from_ldap(group)

        if len(excludes) > 0:
            for person in excludes:
                people.discard(person)

        return people

    def replace_ldap_attr(self, userid, attr, value):
        replace_attr = [(ldap.MOD_REPLACE, attr, value.encode())]
        self.conn.modify_s(
            f'uid={userid},{self.ldap_user_base}',
            replace_attr
        )

    def add_user_to_ldap_group(self, userid, group):
        uid_query = f"uid={userid},{self.ldap_user_base}".encode()
        add_members = [(ldap.MOD_ADD, 'member', uid_query)]
        try:
            self.conn.modify_s(
                f'cn={group},{self.ldap_group_base}',
                add_members
            )
        except (ldap.ALREADY_EXISTS, ldap.TYPE_OR_VALUE_EXISTS):
            pass

    def remove_user_from_ldap_group(self, userid, group):
        uid_query = f"uid={userid},{self.ldap_user_base}".encode()
        remove_members = [(ldap.MOD_DELETE, 'member', uid_query)]
        try:
            self.conn.modify_s(
                f'cn={group},{self.ldap_group_base}',
                remove_members
            )
        except (ldap.NO_SUCH_ATTRIBUTE, ldap.NO_SUCH_OBJECT):
            pass

    def add_or_update_description(self, userid, comment):
        desc = self.get_attributes_from_ldap(userid, 'description')

        if desc:
            _comment = f'{desc}. {comment}'
            update_comment = [
                (ldap.MOD_REPLACE, 'description', _comment.encode())
            ]
            self.conn.modify_s(
                f'uid={userid},{self.ldap_user_base}',
                update_comment
            )
        else:
            update_comment = [(ldap.MOD_ADD, 'description', comment.encode())]
            self.conn.modify_s(
                f'uid={userid},{self.ldap_user_base}',
                update_comment
            )

    def ldap_user_exists(self, userid):
        filter_str = ldap.filter.filter_format('(uid=%s)', (userid,))
        results = self.conn.search_s(
            self.ldap_user_base,
            ldap.SCOPE_SUBTREE,
            filter_str,
            ('uid',)
        )

        return len(results) > 0
