from anthropic import Anthropic
from gitlab.v4.objects.issues import Issue

import os
import json


def generate_json(request: str, issue: Issue) -> str | None:
    system_prompt = """
    You are a request parser that converts natural language GitLab access
    requests into a specific JSON format.

    Rules:
    1. For developer access requests, use:
        {"action": "git_access", "module": "<module_name>"}

    2. For email update requests, use:
        {"action": "update_email"}

    3. Return only the JSON, nothing else.

    4. For module names, extract the exact name mentioned in the request.

    Examples:
    Input: "I want to update my email"
    Output: {"action": "update_email"}

    Input: "Please give me developer access to foobar"
    Output: {"action": "git_access", "module": "foobar"}

    If the request is unclear, no module name is mentioned, or the module name
    is not in the request, return None.

    If the user request is clear enough, make sure the JSON is valid before
    returning it, that also means property names should be enclosed in double quotes.
    Make sure no other actions other than update_email, git_access are processed.
    """

    client = Anthropic(api_key=os.environ.get('ANTHROPIC_API_KEY'))
    r = client.messages.create(
        model="claude-3-haiku-20240307",
        max_tokens=1024,
        system=system_prompt,
        messages=[
            {
                "role": "user",
                "content": request,
            }
        ],
    )

    response = r.content[0].text

    if not response:
        docs = "https://handbook.gnome.org/infrastructure/developer-access.html"
        issue.notes.create(
            {
                "body": (
                    "Your request is unclear. Please follow the guidelines at "
                    f"{docs} and update the issue description accordingly."
                )
            }
        )
        return None

    return json.loads(response)
