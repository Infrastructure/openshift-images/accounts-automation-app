#!/usr/bin/python3

from sqlalchemy import Column, String, Integer, DateTime

from helpers.db.base import Base


class Member(Base):
    __tablename__ = 'members'

    id = Column(Integer, primary_key=True)
    fullname = Column(String(20))
    is_announced = Column(Integer)
    is_emeritus = Column(Integer)
    announced_on = Column(DateTime(timezone=False), nullable=True)

    def __init__(self, fullname, is_announced, is_emeritus, announced_on):
        self.fullname = fullname
        self.is_announced = is_announced
        self.is_emeritus = is_emeritus
        self.announced_on = announced_on
