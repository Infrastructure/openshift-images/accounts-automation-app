CREATE TABLE members (
    id SERIAL PRIMARY KEY,
    fullname VARCHAR(30) NOT NULL,
    is_announced INTEGER NOT NULL,
    is_emeritus INTEGER NOT NULL,
    announced_on TIMESTAMP NULL
);
