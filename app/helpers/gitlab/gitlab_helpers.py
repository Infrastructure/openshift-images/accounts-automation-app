#!/usr/bin/env python3

class GitlabHelpers():
    def __init__(self, GITLAB_API_ENDPOINT, GITLAB_TOKEN, API_VERSION=4):
        import gitlab
        import sys

        self.GITLAB_API_ENDPOINT = GITLAB_API_ENDPOINT
        self.GITLAB_TOKEN = GITLAB_TOKEN
        self.API_VERSION = API_VERSION

        self.gl = gitlab.Gitlab(GITLAB_API_ENDPOINT, GITLAB_TOKEN,
                                api_version=API_VERSION)
        try:
            self.gl.auth()
        except gitlab.exceptions.GitlabAuthenticationError as e:
            print(e)
            sys.exit(1)

    def fetch_gitlab_issues(self, project_id, labels, state):
        project = self.gl.projects.get(project_id)
        issues = project.issues.list(labels=[x for x in labels], state=state)

        return issues

    def fetch_gitlab_issue(self, project_id, issue_id):
        project = self.gl.projects.get(project_id)
        issue = project.issues.get(issue_id)

        return issue

    def fetch_gitlab_issue_thumbs_up(self, project_id, issue_id):
        project = self.gl.projects.get(project_id)
        issue = project.issues.get(issue_id)
        awards = issue.awardemojis.list()

        thumbs_up = []
        for award in awards:
            if award.name == 'thumbsup':
                thumbs_up.append(award.user['id'])

        return thumbs_up

    def fetch_gitlab_issue_thumbs_down(self, project_id, issue_id):
        project = self.gl.projects.get(project_id)
        issue = project.issues.get(issue_id)
        awards = issue.awardemojis.list()

        thumbs_down = []
        for award in awards:
            if award.name == 'thumbsdown':
                thumbs_down.append(award.user['id'])

        return thumbs_down

    def fetch_gitlab_user(self, user_id):
        user = self.gl.users.get(user_id)

        return user

    def fetch_gitlab_user_by_username(self, username):
        try:
            user = self.gl.users.list(username=username)[0]
        except IndexError:
            return None

        return user

    def user_in_gitlab_group(self, user_id, group_id):
        import gitlab

        group = self.gl.groups.get(group_id)

        try:
            group.members.get(user_id)
        except gitlab.exceptions.GitlabGetError:
            return False

        return True

    def close_gitlab_issue(self, project_id, issue_id):
        project = self.gl.projects.get(project_id)
        issue = project.issues.get(issue_id)
        issue.state_event = 'close'
        issue.save()

    def gitlab_comment_already_submitted(self, issue_obj, text):
        _text = text.replace('\r', '').replace('\n', '').strip().encode()

        notes = issue_obj.notes.list()
        for note in notes:
            if note.body.replace('\n', '').strip().encode() == _text:
                return True

        return False

    def find_gitlab_ldap_identity(self, user_id):
        user = self.fetch_gitlab_user(user_id)
        identities = user.identities

        for identity in identities:
            if identity['provider'] == 'ldapmain':
                return identity['extern_uid'].split(',')[0].split('=')[1]

        return None

    def gitlab_comments_by_author(self, issue_obj, user_id):
        notes = issue_obj.notes.list()
        comments = []
        for note in notes:
            if note.author['id'] == user_id:
                comments.append(note)
        return comments

    def list_confidential_notes(self, issue_obj):
        notes = issue_obj.notes.list()
        confidential_notes = []
        for note in notes:
            if note.confidential:
                confidential_notes.append(note)
        return confidential_notes
