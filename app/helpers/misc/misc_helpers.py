#!/usr/bin/env python3

import os
from unidecode import unidecode
import helpers.glu.gnome_ldap_utils as glu

LDAP_GROUP_BASE = os.getenv('LDAP_GROUP_BASE')
LDAP_HOST = os.getenv('LDAP_HOST')
LDAP_USER_BASE = os.getenv('LDAP_USER_BASE')
LDAP_USER = os.getenv('LDAP_USER')
LDAP_PASSWORD = os.getenv('LDAP_PASSWORD')

ldap_utils = glu.GnomeLdapUtils(
    LDAP_GROUP_BASE,
    LDAP_HOST,
    LDAP_USER_BASE,
    LDAP_USER,
    LDAP_PASSWORD
)


class MiscHelpers:
    def generate_gnome_usernames(self, fullname):
        charmap = {
            'ä': 'ae',
            'ö': 'oe',
            'ü': 'ue',
            '-': ''
        }

        for char, replacement in charmap.items():
            if char in fullname:
                fullname = fullname.replace(char, replacement)

        usernames = []
        name_parts = fullname.split()

        first_username = ''
        for i, part in enumerate(name_parts):
            if i == len(name_parts) - 1:
                first_username += part.lower()
            else:
                first_username += part[0].lower()
        usernames.append(unidecode(first_username))

        usernames.append(
            unidecode(name_parts[0].lower() + name_parts[-1][0].lower())
        )

        usernames.append(
            unidecode("".join(name.lower() for name in name_parts))
        )

        usernames.append(
            unidecode(name_parts[0].lower() + name_parts[-1][:2].lower())
        )

        if len(name_parts) > 2:
            usernames.append(
                unidecode("".join(name[0].lower() for name in name_parts))
            )

        usernames = [
            username for username in usernames
            if not ldap_utils.ldap_user_exists(username)
        ]

        return usernames
