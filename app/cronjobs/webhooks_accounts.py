#!/usr/bin/env python3

import os
import sys
from json import loads
from python_freeipa import ClientMeta
from python_freeipa.exceptions import FreeIPAError

import helpers.gitlab.gitlab_helpers as glh
import helpers.misc.misc_helpers as msch
import helpers.glu.gnome_ldap_utils as glu

LDAP_GROUP_BASE = os.getenv('LDAP_GROUP_BASE')
LDAP_HOST = os.getenv('LDAP_HOST')
LDAP_USER_BASE = os.getenv('LDAP_USER_BASE')
LDAP_USER = os.getenv('LDAP_USER')
LDAP_PASSWORD = os.getenv('LDAP_PASSWORD')

ldap_utils = glu.GnomeLdapUtils(
    LDAP_GROUP_BASE, LDAP_HOST, LDAP_USER_BASE, LDAP_USER, LDAP_PASSWORD
)

gitlab_helpers = glh.GitlabHelpers(
    os.getenv('GITLAB_API_ENDPOINT'), os.getenv('GITLAB_TOKEN')
)
gitlab_priv_helpers = glh.GitlabHelpers(
    os.getenv('GITLAB_API_ENDPOINT'), os.getenv('GITLAB_ADMIN_TOKEN')
)

infra_project_id = os.getenv('INFRA_PROJECT_ID')
gnome_project_id = os.getenv('GNOME_PROJECT_ID')

misc_helpers = msch.MiscHelpers()


def main():
    for issue in gitlab_helpers.fetch_gitlab_issues(
        infra_project_id, ['Accounts'], 'opened'
    ):
        gnome_maint_verified = False
        accounts_team_verified = False
        complete = False

        users_thumbs_up = gitlab_helpers.fetch_gitlab_issue_thumbs_up(
            infra_project_id, issue.iid
        )
        if users_thumbs_up:
            for user_id in users_thumbs_up:
                ldap_id = gitlab_priv_helpers.find_gitlab_ldap_identity(user_id)
                if gitlab_helpers.user_in_gitlab_group(user_id, gnome_project_id):
                    gnome_maint_verified = True

                if ldap_id in ldap_utils.get_group_from_ldap('accounts'):
                    accounts_team_verified = True

            if gnome_maint_verified and accounts_team_verified:
                desc = None
                for note in gitlab_priv_helpers.list_confidential_notes(issue):
                    if all(
                        x in note.body for x in ['action', 'git_access', 'module']
                    ):
                        desc = loads(note.body)

                if desc and desc['action'] == 'git_access':
                    user = gitlab_priv_helpers.fetch_gitlab_user(
                        issue.author['id']
                    )
                    user_ldap = gitlab_priv_helpers.find_gitlab_ldap_identity(
                        user.id
                    )

                    if not user_ldap:
                        fullname = user.attributes['name']
                        usernames = misc_helpers.generate_gnome_usernames(fullname)
                        email = user.attributes['email']
                        username = None
                        can_progress = False

                        truncated_fullname = any(
                            user for user in usernames if '.' in user
                        )
                        if truncated_fullname:
                            text = (
                                "Your name or surname in GitLab contain a dot, "
                                "which likely means you truncated your name. "
                                "Please fix your name in GitLab."
                            )
                            if not gitlab_helpers.gitlab_comment_already_submitted(
                                issue, text
                            ):
                                issue.notes.create({'body': text})
                        else:
                            text = (
                                "Your new GNOME Account awaits. Specify a "
                                "username of choice from this list:\n\n"
                                "{usernames}\n\n"
                                "Please add a comment with the username of choice."
                            ).format(
                                usernames="\n".join(
                                    f"1. {username}" for username in usernames
                                )
                            )
                            if not gitlab_helpers.gitlab_comment_already_submitted(
                                issue, text
                            ):
                                issue.notes.create({'body': text})

                            for comment in (
                                gitlab_helpers.gitlab_comments_by_author(
                                    issue, issue.author['id']
                                )
                            ):
                                has_username = any(
                                    comment.body == username
                                    for username in usernames
                                )
                                if has_username:
                                    can_progress = True
                                    username = comment.body
                                    break

                        if can_progress:
                            client = ClientMeta(os.getenv('IDM_SERVER'))
                            client.login(
                                os.getenv('IDM_SA_USER'),
                                os.getenv('IDM_SA_PASSWORD')
                            )
                            try:
                                client.user_add(
                                    username,
                                    o_cn=fullname,
                                    o_mail=email,
                                    o_givenname=fullname.split(' ')[0],
                                    o_sn=fullname.split(' ')[-1]
                                )
                            except FreeIPAError as e:
                                print(e)
                                sys.exit(1)

                            user.provider = 'ldapmain'
                            user.extern_uid = (
                                f"uid={username},{LDAP_USER_BASE}"
                            )
                            user.save()

                            text = (
                                f"GNOME Account created for {username}. "
                                "Reset your password via "
                                "https://password.gnome.org."
                            )
                            issue.notes.create({'body': text})
                    else:
                        username = gitlab_priv_helpers.find_gitlab_ldap_identity(
                            issue.author['id']
                        )

                    if username:
                        if desc['action'] == 'git_access':
                            ldap_utils.add_user_to_ldap_group(
                                username, 'gnomecvs'
                            )
                            ldap_utils.add_or_update_description(
                                username,
                                f"git_access ({issue.web_url})"
                            )
                            text = (
                                f"GNOME GitLab Group access granted for "
                                f"{username}. Permissions will sync in the "
                                "next hour."
                            )
                            issue.notes.create({'body': text})
                            complete = True

                        if complete:
                            gitlab_helpers.close_gitlab_issue(
                                infra_project_id, issue.iid
                            )
                            if not issue.confidential:
                                issue.confidential = True
                                issue.save()


if __name__ == "__main__":
    main()
