#!/usr/bin/env python3

import os
from datetime import date
from helpers.db.db import Member
from helpers.db.base import Session
from pydiscourse import DiscourseClient
from pydiscourse.exceptions import (
    DiscourseClientError, DiscourseError, DiscourseServerError
)


def create_discourse_post(content, title, category):
    api_username = os.getenv('DISCOURSE_API_USERNAME')
    api_key = os.getenv('DISCOURSE_API_KEY')

    client = DiscourseClient(
        'https://discourse.gnome.org',
        api_username=api_username,
        api_key=api_key
    )

    topic = client.create_post(
        content,
        title=title,
        category_id=category
    )

    return topic


def main():
    today = date.today()

    members = Session.query(Member).filter(
        Member.is_announced == 0, Member.is_emeritus == 0
    ).all()
    emeritus = Session.query(Member).filter(
        Member.is_announced == 0, Member.is_emeritus == 1
    ).all()

    title = (
        f"New GNOME Foundation and Emeritus members "
        f"({today.year}-{today.month})"
    )
    category_id = os.getenv('DISCOURSE_CATEGORY_ID')

    content = None
    if members or emeritus:
        content = (
            "Hello! The GNOME Foundation Membership Committee is proud "
            "to announce our newly approved Foundation and Emeritus members. "
            "Please welcome and thank them for their great and valuable "
            "contributions over the GNOME Foundation!"
        )

    if members:
        if content:
            content += '\n\n'

        content += 'New Foundation members are:\n'
        content += '\n'.join(f' - {member.fullname}' for member in members)

        for member in members:
            member.is_announced = 1
            member.announced_on = today

    if emeritus:
        if content:
            content += '\n\n'

        content += 'New Emeritus members are:\n'
        content += '\n'.join(f' - {member.fullname}' for member in emeritus)

        for member in emeritus:
            member.is_announced = 1
            member.announced_on = today

    if content:
        try:
            create_discourse_post(content, title, category_id)
            Session.commit()
        except (DiscourseClientError, DiscourseError, DiscourseServerError):
            Session.rollback()

    Session.remove()


if __name__ == "__main__":
    main()
