#!/usr/bin/env python3

import os
import sys
from datetime import date
from string import Template

import helpers.gitlab.gitlab_helpers as glh
import helpers.misc.misc_helpers as msch
import helpers.glu.gnome_ldap_utils as glu

from helpers.db.db import Member
from helpers.db.base import Session

LDAP_GROUP_BASE = os.getenv('LDAP_GROUP_BASE')
LDAP_HOST = os.getenv('LDAP_HOST')
LDAP_USER_BASE = os.getenv('LDAP_USER_BASE')
LDAP_USER = os.getenv('LDAP_USER')
LDAP_PASSWORD = os.getenv('LDAP_PASSWORD')

ldap_utils = glu.GnomeLdapUtils(
    LDAP_GROUP_BASE, LDAP_HOST, LDAP_USER_BASE, LDAP_USER, LDAP_PASSWORD
)

gitlab_helpers = glh.GitlabHelpers(
    os.getenv('GITLAB_API_ENDPOINT'), os.getenv('GITLAB_TOKEN')
)
gitlab_priv_helpers = glh.GitlabHelpers(
    os.getenv('GITLAB_API_ENDPOINT'), os.getenv('GITLAB_ADMIN_TOKEN')
)

misc_helpers = msch.MiscHelpers()

membctte_project_id = os.getenv('MEMBCTTE_PROJECT_ID')


def main():
    for issue in gitlab_helpers.fetch_gitlab_issues(
        membctte_project_id, [], 'opened'
    ):
        membctte_team_verified = False
        is_membership_renewal = False
        is_emeritus = False
        is_new_member = False
        is_denied = False
        can_progress = False

        users_thumbs_up = gitlab_helpers.fetch_gitlab_issue_thumbs_up(
            membctte_project_id, issue.iid
        )
        users_thumbs_down = gitlab_helpers.fetch_gitlab_issue_thumbs_down(
            membctte_project_id, issue.iid
        )

        if users_thumbs_down:
            for user_id in users_thumbs_down:
                ldap_id = gitlab_priv_helpers.find_gitlab_ldap_identity(user_id)
                if ldap_id in ldap_utils.get_group_from_ldap('membctte'):
                    is_denied = True
                    break

        if is_denied:
            template = 'membership_denied.txt'
            template_path = os.path.join(
                os.getenv('PYTHONPATH'), 'templates', template
            )
            with open(template_path, 'r') as f:
                _template = Template(f.read())
            _template = _template.substitute(cn=issue.author['name'])

            issue.notes.create({'body': _template})
            gitlab_helpers.close_gitlab_issue(membctte_project_id, issue.iid)
            if not issue.confidential:
                issue.confidential = True
                issue.save()
        else:
            if users_thumbs_up:
                for user_id in users_thumbs_up:
                    ldap_id = gitlab_priv_helpers.find_gitlab_ldap_identity(
                        user_id
                    )
                    if ldap_id in ldap_utils.get_group_from_ldap('membctte'):
                        membctte_team_verified = True
                        break

                if membctte_team_verified:
                    user = gitlab_priv_helpers.fetch_gitlab_user(
                        issue.author['id']
                    )
                    user_ldap = gitlab_priv_helpers.find_gitlab_ldap_identity(
                        user.id
                    )

                    if issue.title.lower().find('emeritus') != -1:
                        is_emeritus = True

                    if user_ldap:
                        username = user_ldap
                        fullname = ldap_utils.get_attributes_from_ldap(
                            username, 'cn'
                        )

                        if is_emeritus:
                            ldap_utils.add_user_to_ldap_group(
                                username, 'emeritus'
                            )

                            if username in ldap_utils.get_group_from_ldap(
                                'foundation'
                            ):
                                ldap_utils.remove_user_from_ldap_group(
                                    username, 'foundation'
                                )
                        else:
                            ldap_utils.add_user_to_ldap_group(
                                username, 'foundation'
                            )

                        if username in ldap_utils.get_group_from_ldap(
                            'mailgrace'
                        ):
                            ldap_utils.remove_user_from_ldap_group(
                                username, 'mailgrace'
                            )

                        can_progress = True
                    else:
                        fullname = user.name
                        usernames = misc_helpers.generate_gnome_usernames(fullname)
                        email = user.email

                        truncated_fullname = any(
                            user for user in usernames if '.' in user
                        )
                        if truncated_fullname or len(fullname.split()) == 1:
                            text = (
                                "Your name or surname in GitLab is either a "
                                "nickname or contains a dot, aka it was "
                                "truncated. Please fix your name and surname in "
                                "GitLab. It can be changed back after the "
                                "Foundation membership request process is "
                                "completed. Failure in doing so will result in "
                                "your Foundation membership being denied. "
                            )

                            if not gitlab_helpers.gitlab_comment_already_submitted(
                                issue, text
                            ):
                                issue.notes.create({'body': text})
                        else:
                            text = (
                                "Your Foundation membership awaits, specify an "
                                "username of choice from this list:\n\n"
                                "{usernames}\n\n"
                                "Please add a comment with no other text than "
                                "the username of choice from this list. If your "
                                "current GitLab username is a nickname, please "
                                "make sure your full name is being used. "
                                "Failure in doing so will result in your "
                                "Foundation membership being denied. "
                            ).format(
                                usernames="\n".join(
                                    f"1. {username}" for username in usernames
                                )
                            )
                            if not gitlab_helpers.gitlab_comment_already_submitted(
                                issue, text
                            ):
                                issue.notes.create({'body': text})

                            for comment in gitlab_helpers.gitlab_comments_by_author(
                                issue, issue.author['id']
                            ):
                                if (isinstance(comment.body, int) and
                                        comment.body in range(1, len(usernames) + 1)):
                                    can_progress = True
                                    username = usernames[comment.body - 1]
                                    break

                                if isinstance(comment.body, str):
                                    if any(
                                        comment.body == username
                                        for username in usernames
                                    ):
                                        can_progress = True
                                        username = comment.body
                                        break

                        if can_progress:
                            import python_freeipa
                            client = python_freeipa.ClientMeta(
                                os.getenv('IDM_SERVER')
                            )
                            client.login(
                                os.getenv('IDM_SA_USER'),
                                os.getenv('IDM_SA_PASSWORD')
                            )
                            try:
                                client.user_add(
                                    username,
                                    o_cn=fullname,
                                    o_mail=email,
                                    o_givenname=fullname.split(' ')[0],
                                    o_sn=fullname.split(' ')[-1]
                                )
                            except python_freeipa.exceptions.FreeIPAError as e:
                                print(e)
                                sys.exit(1)

                            ldap_utils.add_user_to_ldap_group(
                                username, 'foundation'
                            )

                            user.provider = 'ldapmain'
                            user.extern_uid = (
                                f'uid={username},{LDAP_USER_BASE}'
                            )
                            user.save()

                            ldap_utils.add_or_update_description(
                                username, issue.web_url
                            )

                    if can_progress:
                        if not is_emeritus:
                            today = date.today().strftime("%Y-%m-%d")

                            if ldap_utils.get_attributes_from_ldap(
                                username, 'FirstAdded'
                            ):
                                ldap_utils.replace_ldap_attr(
                                    username, 'LastRenewedOn', today
                                )
                                is_membership_renewal = True
                            else:
                                ldap_utils.replace_ldap_attr(
                                    username, 'FirstAdded', today
                                )
                                ldap_utils.replace_ldap_attr(
                                    username, 'LastRenewedOn', today
                                )
                                is_new_member = True

                        if is_emeritus:
                            new_member = Member(fullname, 0, 1, None)
                            Session.add(new_member)
                            Session.commit()
                            Session.remove()
                        elif is_new_member:
                            new_member = Member(fullname, 0, 0, None)
                            Session.add(new_member)
                            Session.commit()
                            Session.remove()

                        if is_membership_renewal:
                            template = 'membership_renewal.txt'
                        elif is_emeritus:
                            template = 'emeritus_member.txt'
                        elif is_new_member:
                            template = 'new_foundation_member.txt'

                        template_path = os.path.join(
                            os.getenv('PYTHONPATH'), 'templates', template
                        )
                        with open(template_path, 'r') as f:
                            _template = Template(f.read())

                        if is_new_member:
                            _template = _template.substitute(
                                cn=issue.author['name'], uid=username
                            )
                        else:
                            _template = _template.substitute(
                                cn=issue.author['name']
                            )

                        issue.notes.create({'body': _template})
                        gitlab_helpers.close_gitlab_issue(
                            membctte_project_id, issue.iid
                        )
                        if not issue.confidential:
                            issue.confidential = True
                            issue.save()


if __name__ == "__main__":
    main()
