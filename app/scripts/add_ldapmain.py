#!/usr/bin/env python3

import os
import argparse

from helpers.gitlab import gitlab_helpers as glh

parser = argparse.ArgumentParser(description='Add ldapmain to '
                                 'GitLab username.')
parser.add_argument('--userid', help='GitLab user ID', required=True)
parser.add_argument('--username', help='GNOME Account username', required=True)
args = parser.parse_args()

gl = glh.GitlabHelpers(os.getenv('GITLAB_API_ENDPOINT'),
                       os.getenv('GITLAB_ADMIN_TOKEN'))

user = gl.fetch_gitlab_user(args.userid)
user.provider = 'ldapmain'
user.extern_uid = 'uid={uid},{ldap_user_base}'.format(
    uid=args.username, ldap_user_base=os.getenv('LDAP_USER_BASE')
    )
user.save()
